//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
//#include "pico/stdlib.h"
//#include <pico/float.h>
//#include <pico/double.h>

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */ 

/**
 * @ brief Calculates the approximation of pi using double precision floating point numbers and the walis product
 * 
 * @return aproximation of pi using single precison double point arithemtic and walis product
 * */
float single_precison_floating_pi()
{
    float pi =1.0f;
    for(float n=1; n<=100000; ++n)
    {
        float numerator= 4 * n * n;
        float denominator = numerator-1;
        pi *= (numerator / denominator);
        
        }
    
    pi *= 2.0f;
    return pi;
}

/**
 * @ brief calculates the approximation of pi using single precision floating point numbers and the walis product
 * 
 * @return aproximation of pi using floating point arithemtic and walis product
 * */
double double_precision_floating_pi()
{
    double pi = 1.0;
    for(double n=1; n<=100000; ++n)
    {
        double numerator= 4*n*n;
        double denominator = numerator-1;
        pi *= (numerator/denominator);
    }
    pi *=2.0;
    return pi;
}


int main() {

// print single precision pi value;
float single_precision_wallis = single_precison_floating_pi();
printf("single precision pi: %.10f\n", single_precision_wallis );


double double_precision_wallis = double_precision_floating_pi();
printf("double precision pi: %.10f\n", double_precision_wallis );
float pi_float = 3.14159265359f;
float floataproxerror = pi_float - single_precision_wallis;
float floaterrorpercent = ((single_precision_wallis - pi_float)/pi_float)*100;
if(floaterrorpercent<0)
{
    floaterrorpercent*=-1;
}
printf("single precision floating point pi error: %.10f\n", floataproxerror);
printf("single precision floating point pi error percentage : %.5f\n", floaterrorpercent);


double pi_double = 3.14159265359;
double doubleaproxerror = pi_double - double_precision_wallis;
double doubleerrorpercent = ((double_precision_wallis- pi_double)/pi_double)*100;
if(doubleerrorpercent<0)
{
    doubleerrorpercent*=-1;
}
printf("double precision floating point pi error: %.15f\n", doubleaproxerror);
printf("double precision floating point pi error percentage : %.15f\n", doubleerrorpercent);
#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    //stdio_init_all();
#endif

    // Print a console message to inform user what's going on.
    printf("Hello World!\n");

    // Returning zero indicates everything went okay.
    return 0;
}

