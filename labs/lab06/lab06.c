//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/multicore.h"
#include <inttypes.h>
#include <stdint.h>

#include <math.h>
#include "pico/stdlib.h"
#include <pico/float.h>
#include <pico/double.h>

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */ 




void core1_entry() {
    while (1) {
        // Function pointer is passed to us via the FIFO
        // We have one incoming int32_t as a parameter, and will provide an
        // int32_t return value by simply pushing it back on the FIFO
        // which also indicates the result is ready.
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}

/**
 * @ brief Calculates the approximation of pi using single precision floating point numbers and the walis product
 * 
 * @return aproximation of pi using single precison single point arithemtic and walis product
 * */

float single_precison_floating_pi()
{
    float pi =1.0f;
    for(float n=1; n<=100000; ++n)
    {
        float numerator= 4 * n * n;
        float denominator = numerator-1;
        pi *= (numerator / denominator);
        
        }
    
    pi *= 2.0f;
    return pi;
}

/**
 * @ brief calculates the approximation of pi using double precision floating point numbers and the walis product
 * 
 * @return aproximation of pi using double precision point arithemtic and walis product
 * */
double double_precision_floating_pi()
{
    double pi = 1.0;
    for(double n=1; n<=100000; ++n)
    {
        double numerator= 4*n*n;
        double denominator = numerator-1;
        pi *= (numerator/denominator);
    }
    pi *=2.0;
    return pi;
}


// Function to get the enable status of the XIP cache
bool get_xip_cache_en() {
    return *((io_rw_32 *) XIP_CTRL_BASE) & 1;
}

// Function to set the enable status of the XIP cache
bool set_xip_cache_en(bool cache_en) {
    if (cache_en) {
        hw_set_bits((io_rw_32 *) XIP_CTRL_BASE, 1);
    } else {
        hw_clear_bits((io_rw_32 *) XIP_CTRL_BASE, 1);
    }
    return get_xip_cache_en();
}


int main() {

    

// print single precision pi value;
float single_precision_wallis = single_precison_floating_pi();
printf("single precision pi: %.10f\n", single_precision_wallis );

// calculate and print single precision floating point pi error and error percentage
double double_precision_wallis = double_precision_floating_pi();
printf("double precision pi: %.10f\n", double_precision_wallis );
float pi_float = 3.14159265359f;
float floataproxerror = pi_float - single_precision_wallis;
float floaterrorpercent = ((single_precision_wallis - pi_float)/pi_float)*100;
if(floaterrorpercent<0)
{
    floaterrorpercent*=-1;
}
printf("single precision floating point pi error: %.10f\n", floataproxerror);
printf("single precision floating point pi error percentage : %.5f\n", floaterrorpercent);

// calculate and print double precision floating point pi error and error percentage
double pi_double = 3.14159265359;
double doubleaproxerror = pi_double - double_precision_wallis;
double doubleerrorpercent = ((double_precision_wallis- pi_double)/pi_double)*100;
if(doubleerrorpercent<0)
{
    doubleerrorpercent*=-1;
}
printf("double precision floating point pi error: %.15f\n", doubleaproxerror);
printf("double precision floating point pi error percentage : %.15f\n", doubleerrorpercent);



stdio_init_all();
multicore_launch_core1(core1_entry);
const int  ITER_MAX = 100000;
// Code for sequential run goes here...
// Take snapshot of timer and store
absolute_time_t float_seq_start = get_absolute_time();

// Run the single-precision Wallis approximation

multicore_fifo_push_blocking((uintptr_t) &single_precison_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);
multicore_fifo_pop_blocking();

absolute_time_t float_seq_end = get_absolute_time();
//print the time it takes for float Wallis in sequential mode
printf("Time for float Wallis in sequential mode: %" PRIu64 " microseconds\n", absolute_time_diff_us(float_seq_start, float_seq_end));




// Take snapshot of timer and store
absolute_time_t double_seq_start = get_absolute_time();

// Run the double-precision Wallis approximation
multicore_fifo_push_blocking((uintptr_t) &double_precision_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);

multicore_fifo_pop_blocking();

absolute_time_t double_seq_end = get_absolute_time();

printf("Time for double Wallis in sequential mode: %" PRId64 " microseconds\n", absolute_time_diff_us(double_seq_start, double_seq_end));
// Display time taken for application to run in sequential mode

printf("Time for App in sequential mode: %" PRId64 " microseconds \n", absolute_time_diff_us(float_seq_start, get_absolute_time()));





// Take snapshot of timer and store
absolute_time_t par_start = get_absolute_time();

// Run the single-precision Wallis approximation on one core
multicore_fifo_push_blocking((uintptr_t) &single_precison_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);

// Run the double-precision Wallis approximation on the other core
absolute_time_t start = get_absolute_time();
double_precision_floating_pi(ITER_MAX);
multicore_fifo_pop_blocking();

printf("Time for single Wallis in parallel mode : %" PRId64 " microseconds\n", absolute_time_diff_us(par_start, get_absolute_time()));
printf("Time for double Wallis in parallel mode : %" PRId64 " microseconds\n", absolute_time_diff_us(start, get_absolute_time()));


// Take snapshot of timer and store
absolute_time_t par_end = get_absolute_time();

// Display time taken for application to run in parallel mode
printf("Time for App in parallel mode : %" PRId64 " microseconds\n", absolute_time_diff_us(par_start, par_end));



set_xip_cache_en(true);

// ************** after setting cache calculate the same as above  
absolute_time_t float_seq_start1 = get_absolute_time();

// Run the single-precision Wallis approximation

multicore_fifo_push_blocking((uintptr_t) &single_precison_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);
multicore_fifo_pop_blocking();

absolute_time_t float_seq_end1 = get_absolute_time();

printf(" single Wallis in sequential mode with cache : %" PRIu64 " microseconds\n", absolute_time_diff_us(float_seq_start1, float_seq_end1));




// Take snapshot of timer and store
absolute_time_t double_seq_start1 = get_absolute_time();

// Run the double-precision Wallis approximation
multicore_fifo_push_blocking((uintptr_t) &double_precision_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);

multicore_fifo_pop_blocking();

absolute_time_t double_seq_end1 = get_absolute_time();

printf("double Wallis in sequential mode with cache: %" PRId64 " microseconds\n", absolute_time_diff_us(double_seq_start1, double_seq_end1));
// Display time taken for application to run in sequential mode

printf(" Time for app in sequential mode with cache : %" PRId64 " microseconds \n", absolute_time_diff_us(float_seq_start1, get_absolute_time()));




// Take snapshot of timer and store
absolute_time_t par_start2 = get_absolute_time();

// Run the single-precision Wallis approximation on one core
multicore_fifo_push_blocking((uintptr_t) &single_precison_floating_pi);
multicore_fifo_push_blocking(ITER_MAX);

// Run the double-precision Wallis approximation on the other core
absolute_time_t start2 = get_absolute_time();
double_precision_floating_pi(ITER_MAX);
multicore_fifo_pop_blocking();

printf("Time for single Wallis in parallel mode with cache: %" PRId64 " microseconds\n", absolute_time_diff_us(par_start2, get_absolute_time()));
printf("Time for double Wallis in parallel mode with cache: %" PRId64 " microseconds\n", absolute_time_diff_us(start2, get_absolute_time()));


// Take snapshot of timer and store
absolute_time_t par_end2 = get_absolute_time();

// Display time taken for application to run in parallel mode
printf("Time for App in parallel mode with cache: %" PRId64 " microseconds\n", absolute_time_diff_us(par_start2, par_end2));



#ifndef WOKWI

    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    //stdio_init_all();
#endif

    // Print a console message to inform user what's going on.
    printf("Hello World!\n");

    // Returning zero indicates everything went okay.
    return 0;
}

